#FROM alpine:3.8 AS builder
#
#ARG DEBUG_PORT=4000
#
#ENV GOPATH=/go \
#    PATH=/go/bin:$PATH \
#    DEBUG_PORT=${DEBUG_PORT}
#
#RUN apk update && \
#    apk --no-cache add musl-dev git go && \
#    mkdir -p /go/bin && \
#    go get -u -v github.com/derekparker/delve/cmd/dlv
#
#WORKDIR /go/src/github.com/goingfullstack/currencyconverter0/
#COPY . .
#
#RUN go build -gcflags "all -N -l" "/go/src/github.com/goingfullstack/currencyconverter0"
#
#FROM alpine:3.8
#
#COPY --from=builder "/go/src/github.com/goingfullstack/currencyconverter0/binary" .
#COPY --from=builder /go/bin/dlv .
#
#EXPOSE ${PORT} ${DEBUG_PORT}
#
#CMD ./dlv \
#    --listen=":${DEBUG_PORT}" \
#    --headless=true \
#    --log=true \
#    --api-version=2 \
#    exec "./currencyconverter0"
#



#FROM golang
#
#ARG app_env
#ENV APP_ENV $app_env
#
#COPY ./app /go/src/github.com/user/myProject/app
#WORKDIR /go/src/github.com/user/myProject/app
#
#RUN go get ./
#RUN go build
#
#CMD if [ ${APP_ENV} = production ]; \
#	then \
#	app; \
#	else \
#	go get github.com/pilu/fresh && \
#	fresh; \
#	fi
#
#EXPOSE 8080

FROM golang:onbuild
EXPOSE 8080

### Start from a Debian image with the latest version of Go installed
## and a workspace (GOPATH) configured at /go.
#FROM golang
#
## Copy the local package files to the container's workspace.
#ADD . /go/src/github.com/goingfullstack/currencyconverter0/
#
## Build the outyet command inside the container.
## (You may fetch or manage dependencies here,
## either manually or with a tool like "godep".)
#RUN go install /go/src/github.com/goingfullstack/currencyconverter0
#
## Run the outyet command by default when the container starts.
#ENTRYPOINT /go/bin/currencyconverter0
#
## Document that the service listens on port 8080.
#EXPOSE 4000
#
#CMD exec "./currencyconverter0"